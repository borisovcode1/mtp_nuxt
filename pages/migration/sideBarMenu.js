export default ($router) => {
  return [
    {
      title: 'Методология',
      click: () => $router.push('/mig/met/'),
      variant: 'info',
    },
    {
      title: 'Нормативно-правовое обеспечение',
      click: () => $router.push('/mig/npo/'),
      variant: 'info',
    },
    {
      title: 'Источники данных',
      click: () => $router.push('/mig/doc/'),
      variant: 'danger',
    },
  ]
}
//
// export default ($router) => {
//   return [
//     {
//       title: 'Миграция населения Российской Федерации (Российская Федерация)',
//       click: () => $router.push('/mig/m1/rf'),
//       variant: 'primary',
//     },
//     {
//       title: 'Миграция населения Российской Федерации (Регионы РФ)',
//       click: () => $router.push('/mig/m1'),
//       variant: 'primary',
//     },
//     {
//       title: 'Интеллектуальная миграция (Российская Федерация)',
//       click: () => $router.push('/mig/m2/rf'),
//       variant: 'danger',
//     },
//     {
//       title: 'Интеллектуальная миграция (Регионы РФ)',
//       click: () => $router.push('/mig/m2'),
//       variant: 'danger',
//     },
//     {
//       title: 'Выезд из РФ на постоянное место жительство докторов, кандидатов наук с целью работы по странам',
//       click: () => $router.push('/mig/m2'),
//       variant: 'danger',
//     },
//     {
//       title: 'Большие данные (научная аффиляция)',
//       click: () => $router.push('/mig/m3'),
//       variant: 'success',
//     },
//   ]
// }
