export default {
  chart: {
    type: 'column',
    // animation: Highcharts.svg, // don't animate in old IE
    marginRight: 10,
    // events: {
    //   load() {
    //     // set up the updating of the chart each second
    //     const series = this.series[0]
    //     setInterval(function() {
    //       const x = new Date().getTime() // current time
    //       const y = Math.random()
    //       series.addPoint([x, y], true, true)
    //     }, 1000)
    //   },
    // },
  },

  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        pointFormat: '{point.y:.0f}',
        style: {
          fontSize: '14pt',
        },
      },
    },
  },

  yAxis: {
    title: {
      text: '',
    },
  },

  time: {
    useUTC: false,
  },

  title: {
    text: '',
  },

  tooltip: {
    enabled: false,
    headerFormat: '',
    // pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}',
    pointFormat: '{point.y:.0f}',
  },

  legend: {
    enabled: false,
  },

  exporting: {
    enabled: false,
  },

  series: [],
}
