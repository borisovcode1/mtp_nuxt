export default {
  chart: {
    type: 'spline',
    // animation: Highcharts.svg, // don't animate in old IE
    marginRight: 10,
    // events: {
    //   load() {
    //     // set up the updating of the chart each second
    //     const series = this.series[0]
    //     setInterval(function() {
    //       const x = new Date().getTime() // current time
    //       const y = Math.random()
    //       series.addPoint([x, y], true, true)
    //     }, 1000)
    //   },
    // },
  },

  yAxis: {
    title: {
      text: '',
    },
  },

  time: {
    useUTC: false,
  },

  title: {
    text: '',
  },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        // pointFormat: '{point.y:.2f}',
        pointFormat: '{point.y}',
      },
      label: {
        connectorAllowed: false,
      },
      pointStart: 2010,
    },
  },

  tooltip: {
    enabled: false,
    headerFormat: '',
    // pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}',
    // pointFormat: '{point.value:.2f}',
    pointFormat: '{point.value}',
  },

  legend: {
    enabled: false,
  },

  exporting: {
    enabled: false,
  },

  series: [],
}
