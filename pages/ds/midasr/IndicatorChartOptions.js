export default {
  chart: {
    type: 'spline',
    // animation: Highcharts.svg, // don't animate in old IE
    marginRight: 10,
    // events: {
    //   load() {
    //     // set up the updating of the chart each second
    //     const series = this.series[0]
    //     setInterval(function() {
    //       const x = new Date().getTime() // current time
    //       const y = Math.random()
    //       series.addPoint([x, y], true, true)
    //     }, 1000)
    //   },
    // },
  },

  time: {
    useUTC: false,
  },

  title: {
    text: '',
  },

  // accessibility: {
  //   announceNewData: {
  //     enabled: true,
  //     minAnnounceInterval: 15000,
  //     announcementFormatter(allSeries, newSeries, newPoint) {
  //       if (newPoint) {
  //         return 'New point added. Value: ' + newPoint.y
  //       }
  //       return false
  //     },
  //   },
  // },

  xAxis: {
    type: 'categories',
    tickPixelInterval: 150,
  },

  yAxis: {
    title: {
      text: '',
    },
    plotLines: [
      {
        value: 0,
        width: 1,
        color: '#808080',
      },
    ],
  },

  tooltip: {
    headerFormat: '<b>{series.name}</b><br/>',
    // pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}',
  },

  legend: {
    enabled: false,
  },

  exporting: {
    enabled: false,
  },

  series: [],
}
