export default {
  chart: {
    type: 'column',
  },
  colors: ['#1d70a5', '#7a1c32'],
  title: {
    text: undefined,
  },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        format: '{y:.0f}',
        style: {
          fontSize: 14,
          // color: '#000',
        },
      },
      softThreshold: true,
    },
    // column: {
    //   color: this.colors[1],
    //   lineWidth: 4,
    //   dataLabels: {
    //     // color: '#fff',
    //     // backgroundColor: this.colors[1],
    //     borderColor: '#fff',
    //     backgroundColor: '#ffffffdd',
    //     color: '#000',
    //     padding: 3,
    //     borderWidth: 1,
    //     shadow: false,
    //     style: {
    //       fontWeight: 'bold',
    //       textOutline: '',
    //     },
    //   },
    // },
  },
  xAxis: {
    categories: [],
  },
  yAxis: {
    type: 'linear',
    title: { text: null },
  },
  tooltip: {
    shared: true,
    valueDecimals: 2,
  },
  labels: {},
  series: [],
}
