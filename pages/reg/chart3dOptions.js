export default {
  colors: ['#00e396', '#feb019', '#008ffb', '#a559fa'],
  chart: {
    type: 'column',
    options3d: {
      enabled: true,
      alpha: 20,
      beta: 35,
      depth: 100,
    },
  },
  title: {
    text: '',
  },
  subtitle: {
    text: '',
  },
  plotOptions: {
    column: {
      depth: 120,
      dataLabels: {
        enabled: true,
        style: {
          fontSize: 14,
        },
      },
    },
  },
  xAxis: {
    labels: {
      enabled: false,
    },
    title: {
      text: null,
    },
  },
  yAxis: {
    title: {
      text: null,
    },
  },
  series: [],
}
