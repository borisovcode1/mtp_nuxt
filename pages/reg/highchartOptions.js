export default {
  title: {
    text: undefined,
  },
  legend: {
    enabled: false,
  },
  // colors: ['#00e396', '#feb019', '#008ffb'],
  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        format: '{y:.2f}',
      },
      softThreshold: true,
    },
    spline: {
      lineWidth: 4,
      color: '#153aaa',
      dataLabels: {
        color: '#fff',
        backgroundColor: '#153AAA',
        padding: 3,
        borderColor: '#fff',
        borderWidth: 1,
        shadow: false,
        style: {
          fontWeight: 'bold',
          textOutline: '',
        },
      },
    },
  },
  xAxis: {
    categories: [],
  },
  yAxis: {
    type: 'linear',
    title: {
      text: null,
    },
  },
  tooltip: {
    shared: true,
    valueDecimals: 1,
  },
  labels: {},
  series: [],
}
