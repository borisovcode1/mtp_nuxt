// import webpack from 'webpack'

/** @type {import('highcharts/highcharts').Options} */
const setOptions = {
  lang: {
    loading: 'Загрузка...',
    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    shortMonths: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
    exportButtonTitle: 'Экспорт',
    printButtonTitle: 'Печать',
    rangeSelectorFrom: 'С',
    rangeSelectorTo: 'По',
    rangeSelectorZoom: 'Период',
    downloadPNG: 'Скачать PNG',
    downloadJPEG: 'Скачать JPEG',
    downloadPDF: 'Скачать PDF',
    downloadSVG: 'Скачать SVG',
    printChart: 'Напечатать график',
  },
  credits: {
    enabled: true, // <-- typing "e" suggests "enabled?" ok
  },
}

export default {
  ssr: false,
  /*
   ** Headers of the page
   */
  head: {
    title: 'Информационно-аналитическая платформа Транспортного комплекса города Москвы',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Информационно-аналитическая платформа Транспортного комплекса города Москвы',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    // script: [
    //   {
    //     src: '//code-ya.jivosite.com/widget/EiOWHWl5Jn',
    //     async: true,
    //   },
    // ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: '~/components/loading.vue',
  // loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    // Icofont
    '~assets/icofont/icofont.min.css',
    // Темовский шаблон
    '~assets/base.scss',
  ],
  /*
   ** Plugins to load before mounting the App
   */
  // plugins: [],
  plugins: ['~/plugins/map'],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    // '@nuxtjs/stylelint-module',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://auth.nuxtjs.org
    '@nuxtjs/auth',
    // '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    // '@nuxtjs/dotenv',
    // nuxt-highcharts
    // ['~/highcharts/module.js', { credits: { enabled: false } }],
  ],
  bootstrapVue: {
    componentsPlugin: ['LayoutPlugin', 'FormPlugin', 'FormCheckboxPlugin', 'FormInputPlugin', 'FormRadioPlugin', 'ToastPlugin', 'ModalPlugin'],
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    // baseURL: 'http://localhost:8021/api/v1/',
    // baseURL: 'http://localhost:8303/api/v1/',
    baseURL: 'https://regstat.rea.ru/api/v1/',
  },
  router: {
    middleware: [
      // @nuxtjs/auth middleware
      // 'auth',
      'branch',
    ],
  },
  /*
   ** Auth module configuration
   ** See https://auth.nuxtjs.org
   */
  auth: {
    fetchUserOnLogin: true,
    strategies: {
      local: {
        endpoints: {
          login: { url: '/user/auth/token/login/', method: 'post', propertyName: 'auth_token' },
          logout: { url: '/user/auth/token/logout/', method: 'post' },
          user: { url: '/user/auth/users/me/', propertyName: false },
        },
        tokenType: 'Token',
        tokenName: 'Authorization',
      },
    },
    redirect: {
      login: '/lk/login',
      home: '/',
    },
  },
  /*
   ** Build configuration
   */
  build: {
    // vendor: [
    //   // 'jquery',
    //   'lodash',
    // ],
    // plugins: [
    //   new webpack.ProvidePlugin({
    //     // global modules
    //     // $: 'jquery',
    //     _: 'lodash',
    //   }),
    // ],
  },
}
