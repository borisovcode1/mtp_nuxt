export const state = () => ({
  homePath: '/',
  regName: '',
})

export const mutations = {
  path(state, path) {
    if (path.includes('migration')) {
      state.homePath = '/migration'
    } else if (path.includes('mig')) {
      state.homePath = '/mig'
    } else if (path.includes('belarus')) {
      state.homePath = '/belarus'
    } else if (path === '/') {
      state.homePath = '/'
      state.regName = ''
    }
  },
  setRegName(sate, regName) {
    sate.regName = regName
  },
}
