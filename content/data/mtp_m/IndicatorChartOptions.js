import mtpTs from './mtp_m_ts.json'

export default {
  chart: {
    type: 'spline',
    marginRight: 10,
  },

  time: {
    useUTC: false,
  },

  title: {
    text: '',
  },

  xAxis: {
    type: 'categories',
    categories: mtpTs,
    plotBands: [
      {
        label: {
          text: 'ПРОГНОЗ',
          style: {
            color: '#DC4128',
            fontSize: '14pt',
            fontWeight: 'bold',
          },
        },
        color: '#f9ffab',
        from: 36.5,
        to: 48.5,
      },
    ],
  },

  yAxis: {
    title: {
      text: '',
    },
    plotLines: [
      {
        value: 0,
        width: 1,
        color: '#808080',
      },
    ],
    min: 0,
  },

  tooltip: {
    headerFormat: '<b>{series.name}</b><br/>',
    // pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}',
  },

  legend: {
    enabled: false,
  },

  exporting: {
    enabled: false,
  },

  series: [],
  credits: { enable: false },
}
