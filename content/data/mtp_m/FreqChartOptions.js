import mtpFreq from './mtp_m_freq.json'

export default {
  chart: {
    type: 'line',
    marginRight: 10,
  },

  time: {
    useUTC: false,
  },

  title: {
    text: '',
  },

  xAxis: {
    type: 'categories',
    categories: mtpFreq,
    plotLines: [
      {
        color: '#DC4128',
        value: '19',
      },
    ],
  },

  yAxis: {
    title: {
      text: '',
    },
    plotLines: [
      {
        value: 0,
        width: 1,
        color: '#808080',
      },
    ],
  },

  // tooltip: {
  //   headerFormat: '<b>{series.name}</b><br/>',
  // pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}',
  // },

  legend: {
    enabled: false,
  },

  exporting: {
    enabled: false,
  },

  series: [],
  credits: { enable: false },
}
