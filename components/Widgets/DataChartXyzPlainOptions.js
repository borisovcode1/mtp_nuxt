export default {
  title: {
    text: undefined,
  },
  chart: {
    type: 'bubble',
    zoomType: 'xy',
    animation: true,
  },
  plotOptions: {
    bubble: {
      sizeByAbsoluteValue: false,
      maxSize: '25%',
      minSize: '1%',
      animation: {
        defer: 0,
        duration: 1000,
      },
    },
    series: {
      dataLabels: {
        enabled: false,
        format: '{y:.2f}',
      },
      softThreshold: true,
    },
  },
  tooltip: {
    pointFormat:
      '<strong>{point.name}</strong><br/>' +
      '{point.custom.yIndName}: <strong>{point.y:.2f}</strong><br/>' +
      '{point.custom.xIndName}: <strong>{point.x:.2f}</strong><br/>' +
      '{point.custom.zIndName}: <strong>{point.z:.2f}</strong><br/>' +
      '',
    valueDecimals: 2,
  },
  yAxis: {
    title: {
      text: '',
    },
  },
  series: [],
}
