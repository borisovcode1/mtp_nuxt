export default {
  title: {
    text: undefined,
  },
  chart: {
    type: 'bubble',
    zoomType: 'xy',
  },
  // colors: ['#00e396', '#feb019', '#008ffb'],
  plotOptions: {
    bubble: {
      sizeByAbsoluteValue: false,
      maxSize: '25%',
      minSize: '1%',
    },
    series: {
      dataLabels: {
        enabled: false,
        format: '{y:.4f}',
      },
      softThreshold: true,
    },
  },
  tooltip: {
    pointFormat:
      '<strong>{point.name}</strong><br/>' +
      '{point.custom.yIndName}: <strong>{point.y:.4f}</strong><br/>' +
      '{point.custom.xIndName}: <strong>{point.x:.4f}</strong><br/>' +
      '{point.custom.zIndName}: <strong>{point.z:.4f}</strong><br/>' +
      '',
    valueDecimals: 4,
  },
  yAxis: {
    title: {
      text: '',
    },
  },
  series: [],
}
