import Highcharts from 'highcharts'

export default {
  title: {
    text: undefined,
  },
  chart: {
    type: 'bubble',
    zoomType: 'xy',
    height: 600,
    backgroundColor: {
      linearGradient: [100, 0, 0, 100],
      stops: [
        [0, 'rgb(255, 255, 255)'],
        [1, 'rgb(191,232,255)'],
      ],
    },
  },
  // colors: ['#00e396', '#feb019', '#008ffb'],
  plotOptions: {
    bubble: {
      sizeByAbsoluteValue: false,
      maxSize: '25%',
      minSize: '1%',
      marker: {
        fillColor: {
          radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },
          stops: [
            [0, 'rgba(255,255,255,0.5)'],
            [
              1,
              Highcharts.color(Highcharts.getOptions().colors[0])
                .setOpacity(0.5)
                .get('rgba'),
            ],
          ],
        },
      },
    },
    polygon: {
      enableMouseTracking: false,
    },
    series: {
      dataLabels: {
        enabled: false,
        format: '{y:.2f}',
      },
      softThreshold: true,
    },
  },
  tooltip: {
    headerFormat: '',
    pointFormat:
      '<strong>{point.name}</strong><br/>' +
      '{point.custom.yIndName}: <strong>{point.y:.2f}</strong><br/>' +
      '{point.custom.xIndName}: <strong>{point.x:.2f}</strong><br/>' +
      '{point.custom.zIndName}: <strong>{point.z:.2f}</strong><br/>' +
      '',
    valueDecimals: 2,
  },
  yAxis: {
    title: {
      text: '',
    },
  },
  xAxis: {
    title: {
      text: '',
    },
  },
  legend: {
    enabled: false,
  },
  series: [],
}
