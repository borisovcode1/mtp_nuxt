export default {
  chart: {
    type: 'variablepie',
    backgroundColor: 'none',
    height: 600,
  },
  title: {
    text: null,
  },

  legend: {
    enabled: false,
  },

  pane: {
    size: '80%',
  },

  xAxis: {
    tickmarkPlacement: 'on',
    lineWidth: 0,
    labels: {
      style: {
        color: '#0002f8f',
        cursor: 'default',
        fontSize: '16px',
      },
    },
  },

  yAxis: {
    gridLineInterpolation: 'polygon',
    lineWidth: 0,
    min: 0,
    labels: {
      style: {
        color: '#0000ff',
        cursor: 'default',
        fontSize: '16px',
      },
    },
    gridLineWidth: 2,
    gridLineColor: '#c4c5c5',
  },

  series: [],

  tooltip: {
    headerFormat: '',
    pointFormat: '{point.name}: <b>{point.value:.1f}</b>',
  },

  plotOptions: {
    series: {
      dataLabels: {
        connectorWidth: 5,
        // connectorPadding: 5,
        enabled: true,
        format: '{point.name}: {point.y:.1f}',
        style: {
          color: '#161c77',
          cursor: 'default',
          fontSize: '14pt',
          textAlign: 'center',
        },
      },
    },
  },

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 500,
        },
        chartOptions: {
          legend: {
            align: 'center',
            verticalAlign: 'bottom',
            layout: 'horizontal',
          },
          pane: {
            size: '70%',
          },
        },
      },
    ],
  },
}
