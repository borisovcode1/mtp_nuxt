export default {
  chart: {
    type: 'spline',
    height: 400,
  },
  title: {
    text: null,
  },

  legend: {
    enabled: true,
  },

  // pane: {
  //   size: '80%',
  // },

  xAxis: {
    categories: [],
  },

  yAxis: {
    title: {
      text: '',
    },
  },

  series: [],

  tooltip: {
    headerFormat: '',
    pointFormat: '{point.name}: <b>{point.value:.1f}</b>',
  },

  plotOptions: {
    spline: {
      lineWidth: 4,
      dataLabels: {
        enabled: true,
      },
      enableMouseTracking: true,
    },
  },

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 500,
        },
        chartOptions: {
          legend: {
            align: 'center',
            verticalAlign: 'bottom',
            layout: 'horizontal',
          },
          pane: {
            size: '70%',
          },
        },
      },
    ],
  },
}
