export default {
  ssr: false,
  target: 'static',
  /*
   ** Headers of the page
   */
  head: {
    title: 'СЦ РЭУ им. Г.В.Плеханова',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    // script: [
    //   {
    //     src: '//code-ya.jivosite.com/widget/EiOWHWl5Jn',
    //     async: true,
    //   },
    // ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: '~/components/loading.vue',
  // loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    // Icofont
    '~assets/icofont/icofont.min.css',
    // Темовский шаблон
    '~assets/base.scss',
  ],
  /*
   ** Plugins to load before mounting the App
   */
  // plugins: [],
  plugins: ['~/plugins/map'],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    // '@nuxtjs/stylelint-module',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://auth.nuxtjs.org
    '@nuxtjs/auth',
    // '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    // '@nuxtjs/dotenv',
    [
      '@naumstory/nuxtjs-yandex-metrika',
      {
        id: '67723318',
        webvisor: true,
        clickmap: true,
        // useCDN:false,
        trackLinks: true,
        accurateTrackBounce: true,
      },
    ],
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: '//regstat.rea.ru/api/v1/',
  },
  router: {
    middleware: [
      // @nuxtjs/auth middleware
      // 'auth',
      'branch',
    ],
  },
  /*
   ** Auth module configuration
   ** See https://auth.nuxtjs.org
   */
  auth: {
    fetchUserOnLogin: true,
    strategies: {
      local: {
        endpoints: {
          login: { url: '/user/auth/token/login/', method: 'post', propertyName: 'auth_token' },
          logout: { url: '/user/auth/token/logout/', method: 'post' },
          user: { url: '/user/auth/users/me/', propertyName: false },
        },
        tokenType: 'Token',
        tokenName: 'Authorization',
      },
    },
    redirect: {
      login: '/lk/login',
      home: '/',
    },
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    extractCSS: true,
    optimization: {
      splitChunks: {
        chunks: 'all',
        automaticNameDelimiter: '.',
        name: 'test',
        maxSize: 256 * 1024,
      },
    },
  },
  buildDir: '.nuxt-dist',
  generate: {
    dir: 'public_html',
  },
}
