import json
import csv

data = None
with open('ru.json', encoding='utf-8') as f:
  data = json.load(f)

with open('highmaps.csv', newline='', encoding='utf-8') as f:
  reader = csv.DictReader(f, delimiter=';')
  for row in reader:
    key = row['hc-key']
    short_name = row['short-name']
    name = row['name']
    for item in data['features']:
      if item['properties']['hc-key']==key:
        item['properties']['name'] = name
        item['properties']['short-name'] = short_name
        break

with open('ru-ru.json', 'w', encoding='utf-8') as f:
  json.dump(data, f, ensure_ascii=False)
