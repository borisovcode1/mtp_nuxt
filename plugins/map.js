import Vue from 'vue'
import HighchartsVue from 'highcharts-vue'
import Highcharts from 'highcharts'
import mapInit from 'highcharts/modules/map'
import mapTilemapModule from 'highcharts/modules/tilemap'
import initVariablepie from 'highcharts/modules/variable-pie'
import addRussiaMap from '~/assets/maps/ru-ru'
import addBelarusMap from '~/assets/maps/by-ru'
import addWorldMap from '~/assets/maps/world'

// stockInit(Highcharts)
mapInit(Highcharts)
mapTilemapModule(Highcharts)
initVariablepie(Highcharts)
addRussiaMap(Highcharts)
addBelarusMap(Highcharts)
addWorldMap(Highcharts)

Vue.use(HighchartsVue)

// import VueFusionCharts from 'vue-fusioncharts'
// import FusionCharts from 'fusioncharts'
// import Maps from 'fusioncharts/fusioncharts.maps'
// import Charts from 'fusioncharts/fusioncharts.charts'
// import TimeSeries from 'fusioncharts/fusioncharts.timeseries'
//
// import the theme
// import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion'
// import Russia from './fusioncharts/fusioncharts.russia'
//
// Charts(FusionCharts)
//
// register VueFusionCharts component
// Vue.use(VueFusionCharts, FusionCharts, Maps, Russia)
// Vue.use(VueFusionCharts, FusionCharts, TimeSeries, Maps, Russia, FusionTheme)
