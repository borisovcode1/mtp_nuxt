import Vue from 'vue'

import Default from '~/components/Wrappers/baseLayout.vue'
import Pages from '~/components/Wrappers/pagesLayout.vue'

Vue.component('default-layout', Default)
Vue.component('userpages-layout', Pages)
